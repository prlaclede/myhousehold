import React from "react";
import Card from 'react-bootstrap/Card';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import "./NewMember.css";

export default class NewMember extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            desc:'',
            ff: ''
        }
    }

    handleNameChange = (event) => { this.setState({name: event.target.value});}
    handleDescChange = (event) => { this.setState({desc: event.target.value}); }
    handleFfChange = (event) => { this.setState({ff: event.target.value}); }

    saveUser = () => {
        var postObj = this.state;
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(postObj)
        };
        fetch("/api/members/insert", requestOptions)
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        members: result
                    });
                },
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                (error) => {
                    console.log(error);
                }
            )
    }

    render() {
        return (
            <Card>
                <Card.Body>
                    <Card.Title>Card Title</Card.Title>
                    <Card.Text>
                        <Form onSubmit={this.saveUser}>
                            <Form.Group controlId="name">
                                <Form.Label>Name</Form.Label>
                                <Form.Control type="text" placeholder="Name..." value={this.state.name} onChange={this.handleNameChange} />
                            </Form.Group>

                            <Form.Group controlId="desc">
                                <Form.Label>Description</Form.Label>
                                <Form.Control type="text" placeholder="Description..." value={this.state.desc} onChange={this.handleDescChange} />
                            </Form.Group>

                            <Form.Group controlId="ff">
                                <Form.Label>Favorite Food</Form.Label>
                                <Form.Control type="text" placeholder="Favorite Food.." value={this.state.ff} onChange={this.handleFfChange} />
                            </Form.Group>
                            <Button variant="primary" type="submit">
                                Submit
                            </Button>
                        </Form>
                    </Card.Text>
                </Card.Body>
            </Card>
        );
    }
}