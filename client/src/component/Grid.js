import React, { useEffect, useState } from 'react';
import MemberCard from "./MemberCard";
import NewMember from "./NewMember";
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Button from 'react-bootstrap/Button';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import "./App.css";

export default class Grid extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            newMember: false,
            members: []
        }
    }

    handleClick = () => {
        this.setState(prevState => ({
            newMember: !prevState.newMember
        }));
    }
    
    componentDidMount() { 
        this.getMembers();
    }

    getMembers() {
        fetch("/api/members/getall")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        members: result
                    });
                },
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                (error) => {
                    console.log(error);
                }
            )
    }

    render() {
        return (
            <div className="component-app">
                <Nav activeKey="/">
                    <Nav.Item>
                        <Nav.Link href="/">Marketplace</Nav.Link>
                    </Nav.Item>
                </Nav>
                <Container>
                    <h2>Your household</h2>
                    <p>Welcome to the Marketplace! Review your household below:</p>
                    {this.state.newMember &&
                        <Row>
                            <NewMember />
                        </Row>
                    }
                    <Row>
                        {this.state.members.map(person => (
                            <MemberCard member={person}/>
                        ))}
                    </Row>
                </Container>
                <Button id="addMember" variant="primary" onClick={this.handleClick}>
                    Add Member
                </Button>
            </div>
        );
    }
}