import React from "react";
import Card from 'react-bootstrap/Card';
import "./MemberCard.css";

export default class MemberCard extends React.Component {
    constructor(props) {
        super(props);
      }

    render() {
        console.log(this.props)
        return (
            <Card style={{ width: '18rem' }}>
            <Card.Body>
                <Card.Title>{this.props.member.name}</Card.Title>
                <Card.Subtitle><b>Description:</b> {this.props.member.desc}</Card.Subtitle>
                <Card.Text><b>Favorite Fruit:</b> {this.props.member.ff}</Card.Text>
            </Card.Body>
        </Card>
        );
    }
}