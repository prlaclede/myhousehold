import * as express from 'express';
import * as mongoose from 'mongoose';
import * as logger from "morgan";
import * as bodyParser from "body-parser";
import * as q from "q";
import setRoutes from './routes';

var path = require('path');

const app = express();

app.use(express.json());

app.listen(3100, err => {
  if (err) {
    return console.log(err);
  }
  return console.log('My Express App listening on port 3000');
});

//mount logger
app.use(logger("dev"));

app.use(express.urlencoded({ extended: false }));
//mount json form parser
app.use(bodyParser.json());

//mount query string parser
app.use(bodyParser.urlencoded({
  extended: true
}));

// global.Promise = q.Promise;
// mongoose.Promise = global.Promise;

let mongodbURI = 'mongodb+srv://MyHouseApp:MyHouseApp@cluster0.b5qzh.mongodb.net/MyHouse?retryWrites=true&w=majority'

mongoose.connect(mongodbURI, {useNewUrlParser: true
  // , useUnifiedTopology: true
}).then(db => { 
  console.log('Connected to MongoDB');
  setRoutes(app, db);
  // const collection = db.collection('brandImages.files');
  // const collectionChunks = db.collection('brandImages.chuncks');
})
.catch(err => console.error(err));


export { app }; 