"use strict";
exports.__esModule = true;
var express = require("express");
// import * as request from 'request';
// import * as cors from "cors";
var members_mao_1 = require("./controllers/members.mao");
function setRoutes(app, db) {
    var router = express.Router();
    // //options for cors midddleware
    // const options: cors.CorsOptions = {
    //   allowedHeaders: ["Origin", "X-Requested-With", "Content-Type", "Accept", "X-Access-Token"],
    //   credentials: true,
    //   methods: "GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE",
    //   origin: "http://localhost:4200/",
    //   preflightContinue: false
    // };
    // router.use(cors(options));
    var memberCtrl = new members_mao_1["default"]();
    router.route('/testRouter').get(function () { console.log('router active!'); });
    router.route('/members/getall').get(memberCtrl.getAll);
    router.route('/members/insert').post(memberCtrl.insert);
    // router.options("*", cors(options));
    // Apply the routes to our application with the prefix /api
    app.use('/api', router);
}
exports["default"] = setRoutes;
