"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var members_1 = require("../models/members");
var base_mao_1 = require("./base.mao");
var MembersMAO = /** @class */ (function (_super) {
    __extends(MembersMAO, _super);
    function MembersMAO() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.member = new members_1["default"]();
        _this.model = _this.member.MembersModel;
        return _this;
        // getAllMembers = (req, res) => {
        //     this.model.find({}, (err, members) => {
        //         if (err) {
        //             console.log(err);
        //             res.status(500).json(err);
        //         } else {
        //             res.status(200).json(members);
        //         }
        //     });
        // }
        // saveMember = (req, res) => {
        //     console.log(req.body);
        //     var newMember = new this.member.MembersModel({
        //         name: req.body.name,
        //         desc: req.body.desc,
        //         ff: req.body.ff
        //     });
        //     newMember.save(function (data) {
        //         res.status(200).json(data);
        //     });
        // }
    }
    return MembersMAO;
}(base_mao_1["default"]));
exports["default"] = MembersMAO;
