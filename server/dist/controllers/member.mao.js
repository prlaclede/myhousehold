"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var member_1 = require("../models/member");
var base_mao_1 = require("./base.mao");
var MemberMAO = /** @class */ (function (_super) {
    __extends(MemberMAO, _super);
    function MemberMAO() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.member = new member_1["default"]();
        _this.model = _this.member.MemberModel;
        _this.getAllMembers = function (req, res) {
            // this.model.get((err, members) => {
            //     if (err) {
            //         console.log(err);
            //         res.status(500).json(err);
            //     } else {
            //         res.status(200).json(members);
            //     }
            // });
        };
        _this.saveMember = function (req, res) {
            var newMember = new _this.member.MemberModel();
            newMember.name = req.body.name;
            newMember.description = req.body.description;
            newMember.ff = req.body.ff;
            newMember.save(function (data) {
                res.status(200).json(data);
            });
        };
        return _this;
    }
    return MemberMAO;
}(base_mao_1["default"]));
exports["default"] = MemberMAO;
