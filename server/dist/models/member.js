"use strict";
exports.__esModule = true;
var mongoose = require("mongoose");
var Members = /** @class */ (function () {
    function Members() {
        this.membersSchema = new mongoose.Schema({
            name: String,
            desc: String,
            ff: Date
        });
        this.MembersModel = mongoose.model('Members', this.memberSchema, 'Members');
    }
    Members.prototype.setName = function (name) { this.name = name; };
    Members.prototype.setDesc = function (desc) { this.desc = desc; };
    Members.prototype.setFF = function (ff) { this.ff = ff; };
    return Members;
}());
exports["default"] = Members;
