"use strict";
exports.__esModule = true;
exports.app = void 0;
var express = require("express");
var mongoose = require("mongoose");
var logger = require("morgan");
var bodyParser = require("body-parser");
var routes_1 = require("./routes");
var path = require('path');
var app = express();
exports.app = app;
app.use(express.json());
app.listen(3100, function (err) {
    if (err) {
        return console.log(err);
    }
    return console.log('My Express App listening on port 3000');
});
//mount logger
app.use(logger("dev"));
app.use(express.urlencoded({ extended: false }));
//mount json form parser
app.use(bodyParser.json());
//mount query string parser
app.use(bodyParser.urlencoded({
    extended: true
}));
// global.Promise = q.Promise;
// mongoose.Promise = global.Promise;
var mongodbURI = 'mongodb+srv://MyHouseApp:MyHouseApp@cluster0.b5qzh.mongodb.net/MyHouse?retryWrites=true&w=majority';
mongoose.connect(mongodbURI, { useNewUrlParser: true
    // , useUnifiedTopology: true
}).then(function (db) {
    console.log('Connected to MongoDB');
    routes_1["default"](app, db);
    // const collection = db.collection('brandImages.files');
    // const collectionChunks = db.collection('brandImages.chuncks');
})["catch"](function (err) { return console.error(err); });
