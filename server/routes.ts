import * as express from 'express';
// import * as request from 'request';

// import * as cors from "cors";

import MemberCtrl from './controllers/members.mao';
import Member from 'models/members';

export default function setRoutes(app, db) {

  const router = express.Router();

  // //options for cors midddleware
  // const options: cors.CorsOptions = {
  //   allowedHeaders: ["Origin", "X-Requested-With", "Content-Type", "Accept", "X-Access-Token"],
  //   credentials: true,
  //   methods: "GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE",
  //   origin: "http://localhost:4200/",
  //   preflightContinue: false
  // };

  // router.use(cors(options));

  const memberCtrl = new MemberCtrl();

  router.route('/testRouter').get(function () { console.log('router active!'); });

  router.route('/members/getall').get(memberCtrl.getAll);
  
  router.route('/members/insert').post(memberCtrl.insert);

  // router.options("*", cors(options));

  // Apply the routes to our application with the prefix /api
  app.use('/api', router);

}