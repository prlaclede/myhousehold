import * as mongoose from 'mongoose';

export default class Members {
    constructor() { }

    name: String;
    desc: String;
    ff: String;

    public setName(name) { this.name = name; }

    public setDesc(desc) { this.desc = desc; }

    public setFF(ff) { this.ff = ff; }

    private membersSchema = new mongoose.Schema({
        name: String,
        desc: String,
        ff: String
    });

    public MembersModel = mongoose.model('Members', this.membersSchema, 'Members');
}