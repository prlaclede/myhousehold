# My House Application

This is a version of the My House coding assignment using react on the front end, and an express server to read/write to a mongodb cloud DB

## Installation

Installation should be covered in a few steps
- Install, build, run the client
  - ``` cd myhouse/client ```
  - ``` npm install ```
  - ``` npm start ```
- Install, build, run the server
  - ``` cd myhouse/server ```
  - ``` npm install ```
  - ``` npm start ```


## Overview and Description

### React
I decided to go with React for the front end design, mainly for 2 reasons
- I've never really worked with React, but I have done a good amount of Angular, so I figured this would be a good time to learn and translate that knowledge
- It was mentioned Nava uses React a fair amount, so all the more better to learn

### Bootstrap
I went with Bootstrap for some basic styles on the front end
 - Mainly this was picked because it's just very easy to work with
 - I have a good amount of experience with bootstrap as well, so using the components and classes was quick and didn't require too much research.
 - I knew I could pretty easily emulate the example image, and leverage the layout system in Bootstrap to achieve a responsive design. 

### Express
I decided to go a bit above and include a webserver that could read/write into a database. 
 - I already had a template for this whole server as part of another side project I wrote, so modifying it to be used here was pretty easy.
 - Mongo DB is something I'm enjoying playing with/learning, so I figured it couldn't hurt to get a bit more experience.
 - MongoDB Atlas is really pretty easy to work with for smaller apps or proof of concept stuff

### Improvements/Considerations
Of course there is a lot that could be improved upon here
 - While the app supports read/write, it could support edit/delete, but does not at this time
 - There is absolutely no thought of security in this case
   - The DB connection (username/password) is stored in plain text in a file that goes into the repo
   - This of course would need to be handled differently in a real world scenario 
 - There are likely a few UI/style improvements that could be made
   - Most the components and styling are lightly modified bootstrap components
 - There is no scalability
   - The page would likely become unusable after creating 20+ members, as I didn't implement paging, or any form of organizing the data on the page

## Final Thoughts

Overall I thought this was a good way to look at and measure skills. It was nice you point to frameworks like [Create React App](https://reactjs.org/docs/create-a-new-react-app.html#create-react-app) as spinning up frameworks like this can take an hour or so alone if you've never worked with them. I think this assignment could be more interesting if you had an existing cloud DB that you required I/O into, or maybe a SQLite db you could provide a framework for accessing. Overall I thought this was a fair assessment though. 
